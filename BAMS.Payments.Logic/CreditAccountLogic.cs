﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Exceptions;
using BAMS.Payments.Core.Helpers;
using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Core.Interfaces;
using BAMS.Payments.Data;
using BAMS.Payments.Data.Hibernate.Implementation;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Logic
{
    public class CreditAccountLogic
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private ICreditAccountRepo _repository = SafeServiceLocator<ICreditAccountRepo>.GetService();


        public ICreditAccount Get(long id)
        {
            ICreditAccount result = null;
            result = _repository.Get(_theDataSource, id);
            return result;


        }
      
        public ICreditAccount Save(ICreditAccount transaction)
        {

            try
            {
                _repository.DbContext.BeginTransaction(_theDataSource);
                var result = _repository.Save(_theDataSource, transaction);
                _repository.DbContext.CommitTransaction(_theDataSource);
                return result;
            }
            catch (Exception ex)
            {
                _repository.DbContext.RollbackTransaction(_theDataSource);
                throw ex;
            }

        }

        public ICreditAccount Update(ICreditAccount inst)
        {
            try
            {
                _repository.DbContext.BeginTransaction(_theDataSource);
                var result = _repository.Update(_theDataSource, inst);
                _repository.DbContext.CommitTransaction(_theDataSource);

                return result;
            }
            catch (Exception ex)
            {
                _repository.DbContext.RollbackTransaction(_theDataSource);
                throw ex;
            }
        }

    }
}
    