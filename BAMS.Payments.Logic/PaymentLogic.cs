﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Exceptions;
using BAMS.Payments.Core.Helpers;
using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Core.Interfaces;
using BAMS.Payments.Data;
using BAMS.Payments.Data.Hibernate.Implementation;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Logic
{
    public class PaymentLogic
    {
        private IDataSource _theDataSource = DataSourceFactory.GetDataSource(DataCategory.Core);
        private ITransferRequestRepo _repository = SafeServiceLocator<ITransferRequestRepo>.GetService();


        public ITransferRequest Get(long id)
        {
            ITransferRequest result = null;
            result = _repository.Get(_theDataSource, id);
            return result;


        }
        public ITransferRequest GetByReference(string reference)
        {
            ITransferRequest result = null;
            result = _repository.GetByReference(_theDataSource, reference);
            return result;


        }
        public IList<ITransferRequest> GetPendingTransactions()
        {
            return _repository.GetPendingTransactions(_theDataSource);
        }
        public ITransferRequest Save(ITransferRequest transaction)
        {

            try
            {
                _repository.DbContext.BeginTransaction(_theDataSource);
                var result = _repository.Save(_theDataSource, transaction);
                _repository.DbContext.CommitTransaction(_theDataSource);
                return result;
            }
            catch (Exception ex)
            {
                _repository.DbContext.RollbackTransaction(_theDataSource);
                throw ex;
            }

        }

        public ITransferRequest Update(ITransferRequest inst)
        {
            try
            {
                _repository.DbContext.BeginTransaction(_theDataSource);
                var result = _repository.Update(_theDataSource, inst);
                _repository.DbContext.CommitTransaction(_theDataSource);

                return result;
            }
            catch (Exception ex)
            {
                _repository.DbContext.RollbackTransaction(_theDataSource);
                throw ex;
            }
        }
        public bool Authenticate(AuthenticationHeader header)
        {
            bool isAuthorized = false;
            string computedHash = "";
            try
            {
                bool enableAuthentication = string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableAuthentication"]) ? true : Convert.ToBoolean(ConfigurationManager.AppSettings["EnableAuthentication"]);

                if (!enableAuthentication)
                {
                    Logger.Log("Authentication Disabled");
                    return true;
                }
                if (header == null || string.IsNullOrEmpty(header.ClientCode) || string.IsNullOrEmpty(header.Mac) || string.IsNullOrEmpty(header.TimeStamp)) return isAuthorized;

                string secret = ConfigurationManager.AppSettings["SecretKey"];
                string clientCode = ConfigurationManager.AppSettings["ClientCode"];

                string value = $"{clientCode}|{secret}|{header.TimeStamp}";
                Logger.Log($"Raw: {value}");
                var data = System.Text.Encoding.UTF8.GetBytes(value);
                using (SHA512 shaM = SHA512.Create())
                {
                    var hashBytes = shaM.ComputeHash(data);
                    computedHash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
                }
                Logger.Log($"Computed Hash {computedHash}");
                Logger.Log($"Received Mac {header.Mac}");

                if (string.Equals(header.Mac, computedHash))
                {
                    isAuthorized = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, $"Authorization Error");
                return isAuthorized;
            }
            return isAuthorized;
        }

        public AccountEnquiryResponse NameEnquiry(AccountEnquiryRequest request)
        {
            return new CoreBankingProcessor().AccountEnquiry(request, false);
        }
        public AccountEnquiryResponse BalanceEnquiry(AccountEnquiryRequest request)
        {
            return new CoreBankingProcessor().AccountEnquiry(request, true);
        }
        public TransferResponse LogTransferRequest(ITransferRequest request)
        {
            TransferResponse response = new TransferResponse { transref = request.transref };
            string paymentRef = string.Empty;
            try
            {
                //check if transref exists
                var existing = GetByReference(request.transref);
                if (existing != null) throw new FailureException("Duplicate transaction reference", "12");

                //check if its debit or credit to determine the ttype
                if (new CoreBankingProcessor().CheckType(request.debitaccountnumber, out TransactionType ttype))
                {
                    request.transactionType = ttype;
                    Logger.Log($"Ttype for {request.transref} is {request.transactionType.ToString()}");
                }
                else
                {
                    Logger.Log("Unable to determine the transaction type");
                    response.IsSuccessful = true;
                    response.status = "96";
                    response.statusdescription = "System Error";
                    response.paymentref = paymentRef;
                    return response;
                }

               
                //generate payment ref
                paymentRef = request.creditaccounts.Count() + string.Format("{0:yyyyMMddHHmmssff}", DateTime.Now);
                request.paymentref = paymentRef;
                request.Status = TransactionStatus.ReadyForProcessing;
                request.LogDate = DateTime.Now;

                foreach (var item in request.creditaccounts)
                {
                    item.status = "06";
                    item.statusdescription = "In Progress";
                    item.TransferRequest = request;
                    item.TransactionStatus = TransactionStatus.Pending;
                }
                Save(request);

                response.IsSuccessful = true;
                response.status = "16";
                response.statusdescription = "Successful";
                response.paymentref = paymentRef;
            }
            catch (FailureException ex)
            {
                response.status = ex.Code;
                response.statusdescription = ex.Message;
                Logger.Log(ex);
            }
            catch (Exception ex)
            {
                response.status = "97";
                response.statusdescription = "System Error";
                Logger.Log(ex);
            }
            finally
            {
                response.responsetimestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }

            return response;
        }

        public void ProcessLoggedTransaction()
        {
            //get all pending batch transaction
            //in each request, check if transaction is pending
            //if pending , update to processing
            //depending of the ttype of the batch, send to cba
            //when response is received
            // if successful, update status and status description to sucees, Processed
            //if failed, update to failed, Failed
            //if no response, leave transaction at Processing?? for dispute,
            //when all transaction has been processed, update batch to processed
            var pendingTransactions = GetPendingTransactions();
            Logger.Log($"Pending Transactions {pendingTransactions?.Count()}");

            if (pendingTransactions != null && pendingTransactions.Count() > 0)
            {
                try
                {
                    foreach (var transaction in pendingTransactions)
                    {
                        transaction.Status = TransactionStatus.Processing;
                        Update(transaction);

                        Logger.Log($"No of Items in batch {transaction.creditaccounts?.Count()}");
                        Response response = null;


                        if (transaction.creditaccounts != null && transaction.creditaccounts.Count() != 0)
                        {
                            if (transaction.transactionType == TransactionType.Debit)
                            {
                                try
                                {
                                  
                                    response = new CoreBankingProcessor().Debit(transaction, transaction.creditaccounts);

                                    foreach (var item in transaction.creditaccounts)
                                    {
                                        item.status = response.ResponseCode;
                                        item.statusdescription = response.ResponseMessage;
                                        item.TransactionStatus = response.Status;

                                        // if respose is processing then do TSQ
                                        if (response.Status == TransactionStatus.Processing)
                                        {
                                            Logger.Log("Unable to determine the status of the transaction");
                                            item.status = "06";
                                        }
                                    }
                                }
                                finally
                                {
                                }
                               
                            }
                            else
                            {
                                foreach (var item in transaction.creditaccounts)
                                {
                                    try
                                    {


                                        if (item.TransactionStatus != TransactionStatus.Pending)
                                        {
                                            continue;
                                        }

                                        Logger.Log("Updating transaction before processing ");
                                        item.TransactionStatus = TransactionStatus.Processing;
                                        item.transactionRef = transaction.paymentref + "_" + item.serialno;
                                        new CreditAccountLogic().Update(item);
                                        Logger.Log("Done updating transaction before processing ");


                                        if (transaction.transactionType == TransactionType.Credit)
                                        {
                                            response = new CoreBankingProcessor().Credit(transaction, item);
                                        }
                                        

                                        item.status = response.ResponseCode;
                                        item.statusdescription = response.ResponseMessage;
                                        item.TransactionStatus = response.Status;

                                        // if respose is processing then do TSQ
                                        if (response.Status == TransactionStatus.Processing)
                                        {
                                            Logger.Log("Unable to determine the status of the transaction");
                                            item.status = "06";
                                        }


                                    }
                                    finally
                                    {
                                        new CreditAccountLogic().Update(item);
                                    }

                                }
                            }
                            transaction.Status = TransactionStatus.Processed;
                            Update(transaction);
                        }
                        else
                        {
                            transaction.Status = TransactionStatus.Suspended;
                            transaction.ResponseMessage = "No items to processs";
                            Update(transaction);
                        }

                    }

                }
                finally
                {

                }
            }
        }
        public TransferResponse TSQ(TSQRequest request)
        {
            TransferResponse response = new TransferResponse();

            try
            {
                var transferRequest = GetByReference(request.transref);

                if (transferRequest == null) throw new FailureException("Invalid Reference", "01");

                response = new TransferResponse { transref = transferRequest.transref, creditaccounts = transferRequest.creditaccounts, paymentref = transferRequest.paymentref, responsetimestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") };
            }
            catch (FailureException ex)
            {
                response.status = ex.Code;
                response.statusdescription = ex.Message;
                Logger.Log(ex);
            }
            catch (Exception ex)
            {
                response.status = "97";
                response.statusdescription = "System Error";
                Logger.Log(ex);
            }

            return response;
        }
    }
}
