﻿using FluentNHibernate.Automapping;
using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Data.Hibernate.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Logic.Initializer
{
    public class ApplicationInitializer
    {
        public static void Init()
        {


            Logger.Log("About to commence");


            AutoPersistenceModel autoPersistenceModel = new AutoPersistenceModel();
            //autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            //autoPersistenceModel.Conventions.Add<ClassMappingConvention>();
            //autoPersistenceModel.Conventions.Add<ClassMappingConvention>();

            HibernateHelper.Init(new SimpleSessionStorage(),
   new string[] { System.Configuration.ConfigurationManager.AppSettings["CoreMappingAssembly"] },
                    autoPersistenceModel,null,null,DataSourceFactory.GetDataSource(DataCategory.Core)
                 );
            Logger.Log("finished nhibernate ");

           
        }

    }
}
