﻿using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CuttingEdge.ServiceLocation;
using System.Threading.Tasks;
using BAMS.Payments.Data;
using BAMS.Payments.Data.Implementation;

namespace BAMS.Payments.Logic.Initializer
{
    public class ServiceInitialization : SimpleServiceLocator
    {
        public ServiceInitialization() : base()
        {
            this.RegisterSingle<IDbContext>(BAMS.Payments.Data.Hibernate.Implementation.DbContext.Instance);
            this.RegisterSingle<ITransferRequestRepo>(new TransferRequestRepo());
            this.RegisterSingle<ICreditAccountRepo>(new CreditAccountRepo());
           



        }
    }
}
