﻿using Newtonsoft.Json;
using BAMS.Payments.Core.Exceptions;
using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BAMS.Payments.Core.Enums;

namespace BAMS.Payments.Logic
{
    public class CoreBankingProcessor
    {
        public static string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"] ?? "http://155.93.117.4:9030/UpperLink/";

        public AccountEnquiryResponse AccountEnquiry(AccountEnquiryRequest request, bool isBalanceCheck)
        {
            AccountEnquiryResponse response = new AccountEnquiryResponse { accountnumber = request.accountnumber };
            try
            {
                string url = BaseURL + (isBalanceCheck ? $"Balance" : "NameEnquiry");
                var theObject = new
                {
                    accountnumber = request.accountnumber,

                };
                var theResponse = UrlPost(url, theObject);

                if (String.IsNullOrEmpty(theResponse)) throw new FailureException("Invalid response from cba");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(theResponse);
                if (result == null) throw new FailureException("Invalid response");

                string value = string.Empty;


                result.TryGetValue("balance", out value);
                response.balance = value;
                result.TryGetValue("accountname", out value);
                response.accountname = value;
                response.responsetimestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                response.IsSuccessful = true;



            }
            catch (FailureException ex)
            {
                Logger.Log(ex);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }

            return response;
        }

        public Response Debit(ITransferRequest request, IList<CreditAccount> creditAccount)
        {
            Response response = new Response();
            try
            {
                string url = BaseURL + $"PostDebitsUpperLink";
                var theObject = new
                {
                    debitaccountnumber = request.debitaccountnumber,
                    debitbankcode = request.debitbankcode,
                    transref = request.transref,
                    serialno = creditAccount.FirstOrDefault()?.serialno,
                    accountnumber = creditAccount.FirstOrDefault()?.accountnumber,
                    bankcode = creditAccount.FirstOrDefault()?.bankcode,
                    amount = creditAccount.Sum(x=>x.amount),
                    narration = creditAccount.FirstOrDefault()?.narration,
                    paymentref = request.transref
                };
                var theResponse = UrlPost(url, theObject);

                if (String.IsNullOrEmpty(theResponse)) throw new FailureException("Invalid response from cba");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(theResponse);
                if (result == null) throw new FailureException("No response from cba");

                string value = string.Empty;


                if (result.TryGetValue("result", out value) && Convert.ToBoolean(value))
                {
                    response.IsSuccessful = true;
                    response.ResponseCode = "00";
                    response.Status = Core.Enums.TransactionStatus.Successful;

                }
                else
                {
                    result.TryGetValue("errorMessage", out value);
                    response.ResponseMessage = value;
                    result.TryGetValue("responseCode", out value);
                    response.ResponseCode = string.IsNullOrEmpty(value) ? "97" : value; 
                    response.Status = Core.Enums.TransactionStatus.Failed;
                }


            }
            catch (FailureException ex)
            {
                response.ResponseMessage = ex.Message;
                response.ResponseCode = "97";
                response.Status = Core.Enums.TransactionStatus.Processing;
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ex.Message;
                response.ResponseCode = "97";
                response.Status = Core.Enums.TransactionStatus.Processing;
            }

            return response;
        }
        public Response Credit(ITransferRequest request, ICreditAccount creditAccount)
        {
            Response response = new Response();
            try
            {
                string url = BaseURL + $"PostCreditsUpperLink";
                var theObject = new
                {
                    debitaccountnumber = request.debitaccountnumber,
                    debitbankcode = request.debitbankcode,
                    transref = request.transref,
                    serialno = creditAccount.serialno,
                    accountnumber = creditAccount.accountnumber,
                    bankcode = creditAccount.bankcode,
                    amount = creditAccount.amount,
                    narration = creditAccount.narration,
                    paymentref = creditAccount.transactionRef
                };
                var theResponse = UrlPost(url, theObject);

                if (String.IsNullOrEmpty(theResponse)) throw new FailureException("Invalid response from cba");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(theResponse);
                if (result == null) throw new FailureException("No response from cba");

                string value = string.Empty;


                if (result.TryGetValue("result", out value) && Convert.ToBoolean(value))
                {
                    response.IsSuccessful = true;
                    response.ResponseCode = "00";
                    response.Status = Core.Enums.TransactionStatus.Successful;

                }
                else
                {
                    result.TryGetValue("errorMessage", out value);
                    response.ResponseMessage = value;
                    result.TryGetValue("responseCode", out value);
                    response.ResponseCode = string.IsNullOrEmpty(value)?"97":value;
                    response.Status = Core.Enums.TransactionStatus.Failed;
                }


            }
            catch (FailureException ex)
            {
                response.ResponseMessage = ex.Message;
                response.ResponseCode = "97";
                response.Status = Core.Enums.TransactionStatus.Processing;
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ex.Message;
                response.ResponseCode = "97";
                response.Status = Core.Enums.TransactionStatus.Processing;
            }

            return response;
        }
        public bool CheckType(string accountNumber, out TransactionType transactionType)
        {
            bool isSuccessful = false;
            transactionType = 0;
            try
            {
                string url = BaseURL + $"CheckInflowOutflow";
                var theObject = new
                {
                    accountnumber = accountNumber,

                };
                var theResponse = UrlPost(url, theObject);

                if (String.IsNullOrEmpty(theResponse)) throw new FailureException("Invalid response from cba");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(theResponse);
                if (result == null) throw new FailureException("Invalid response");

                string value = string.Empty;


                if (result.TryGetValue("transfertype", out value))
                {
                    if (value.ToLower() == "inflow")
                    {
                        transactionType = TransactionType.Credit;
                        isSuccessful = true;
                    }
                    else if (value.ToLower() == "outflow")
                    {
                        transactionType = TransactionType.Debit;
                        isSuccessful = true;
                    }
                }



            }
            catch (FailureException ex)
            {
                Logger.Log(ex);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return isSuccessful;
        }
        public string UrlGet(string url)
        {
            string result = "";

            try
            {


                Logger.Log($"Getting  URL {url}");
                using (var client = new HttpClient())
                {
                    var authToken = Encoding.ASCII.GetBytes($"DemoUser1:pass123");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                            Convert.ToBase64String(authToken));
                    using (var resp = client.GetAsync(url).Result)
                    {
                        if (!resp.IsSuccessStatusCode) throw new Exception(resp.Content.ReadAsStringAsync().Result);

                        Logger.Log($"Getting  URL {url} Status: Successful");

                        result = resp.Content.ReadAsStringAsync().Result;

                    }
                }

                Logger.Log($"Done Getting from URL {url}, \n Response {result}");

                return result;

            }
            catch (TimeoutException ex)
            {

                Logger.Log(ex);
                throw new Exception("Error processing request");
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw ex;
            }

        }

        public string UrlPost(string url, object theObject)
        {
            try
            {

                string result = string.Empty;
                string obj = JsonConvert.SerializeObject(theObject);
                Logger.Log($"@Request {obj} - Url :{url} ");
                StringContent content = new StringContent(obj, Encoding.UTF8, "application/json");
                using (var client = new HttpClient())
                {
                    var authToken = Encoding.ASCII.GetBytes($"DemoUser1:pass123");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                            Convert.ToBase64String(authToken));

                    using (var resp = client.PostAsync(url, content).Result)
                    {
                        if (!resp.IsSuccessStatusCode) throw new Exception(resp.Content.ReadAsStringAsync().Result);


                        result = resp.Content.ReadAsStringAsync().Result;

                    }
                }

                Logger.Log($"Done Posting   URL {url}, \n Response {result}");

                return result;

            }
            catch (TimeoutException ex)
            {

                Logger.Log(ex);
                throw new Exception("Error processing request");
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw ex;
            }

        }

    }
}
