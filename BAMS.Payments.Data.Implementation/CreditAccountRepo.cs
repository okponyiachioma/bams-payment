﻿using BAMS.Payments.Core.Interfaces;
using BAMS.Payments.Data.Hibernate.Interfaces;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Implementation
{
   public class CreditAccountRepo: Repository<ICreditAccount>, ICreditAccountRepo
    {
        public ICreditAccount GetByReference(IDataSource source, string reference)
        {
            ICriteria criteria = Session(source).CreateCriteria(typeof(ICreditAccount));
            criteria.Add(Restrictions.Eq("transref", reference));
            return criteria.List<ICreditAccount>()?.FirstOrDefault();
        }
    }
}
