﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Interfaces;
using BAMS.Payments.Data.Hibernate.Interfaces;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Implementation
{
    public class TransferRequestRepo : Repository<ITransferRequest>, ITransferRequestRepo
    {
        public ITransferRequest GetByReference(IDataSource source, string reference)
        {
            ICriteria criteria = Session(source).CreateCriteria(typeof(ITransferRequest));
            criteria.Add(Restrictions.Eq("transref", reference));
            return criteria.List<ITransferRequest>()?.FirstOrDefault();
        }
        public IList<ITransferRequest> GetPendingTransactions(IDataSource source)
        {
            ICriteria criteria = Session(source).CreateCriteria(typeof(ITransferRequest));
            criteria.Add(Restrictions.Eq("Status", TransactionStatus.ReadyForProcessing));
            return criteria.List<ITransferRequest>().ToList();
        }
    }
}
