﻿using FluentNHibernate.Mapping;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Mapping
{
    public class EntityMap<T> : ClassMap<T> where T : IEntity
    {
        public EntityMap()
        {
            Id(x => x.ID)
                .GeneratedBy.Native();
        }
    }
}
