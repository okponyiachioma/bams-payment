﻿using BAMS.Payments.Core.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Mapping
{

    public class TransferMap : EntityMap<TransferRequest>
    {
        public TransferMap()
        {
            Map(x => x.transref);
            Map(x => x.debitaccountnumber);
            Map(x => x.debitbankcode);
            Map(x => x.mfbhandlesdebit);
            Map(x => x.description);
            Map(x => x.LogDate);         
            Map(x => x.Status);           
            Map(x => x.paymentref);
            Map(x => x.transactionType);
            HasMany<CreditAccount>(x => x.creditaccounts).Inverse().Not.LazyLoad().Cascade.All().KeyColumn("TransferRequestID");




        }

    }


}
