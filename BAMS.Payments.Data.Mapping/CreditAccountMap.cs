﻿using BAMS.Payments.Core.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Mapping
{

    public class CreditAccountMap : EntityMap<CreditAccount>
    {
        public CreditAccountMap()
        {

            Map(x => x.accountnumber);         
            Map(x => x.bankcode);           
            Map(x => x.narration);
            Map(x => x.serialno);
            Map(x => x.amount);
            Map(x => x.status);
            Map(x => x.statusdescription);
            Map(x => x.TransactionStatus);
            Map(x => x.transactionRef);
            References<TransferRequest>(x => x.TransferRequest, "TransferRequestID");



        }

    }
  

}
