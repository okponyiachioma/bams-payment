﻿using BAMS.Payments.Core.Interfaces;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data
{
   public interface ICreditAccountRepo : IRepository<ICreditAccount>
    {
        //ICreditAccount GetByReference(IDataSource source, string reference);

    }
}
