﻿using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data
{
    /// <summary>
    /// Provides a standard interface for DAOs which are data-access mechanism agnostic.
    /// 
    /// Since nearly all of the domain objects you create will have a type of long ID, this 
    /// base IDao leverages this assumption.  If you want an entity with a type 
    /// other than int, such as string, then use <see cref="IRepositoryWithTypedId{T, IdT}" />.
    /// </summary>
    public interface IRepository<T> : IRepositoryWithTypedId<T, long> { }

    /// <summary>
    /// Basic(object) Interface for DAOs with unknown/dynamic entity types
    /// </summary>
    public interface IRepository : IRepository<object>
    {

    }

    /// <summary>
    /// Standard Interface for Data Access with basic CRUD methods
    /// </summary>
    /// <typeparam name="T">Entity Type</typeparam>
    /// <typeparam name="IdT">Type of the entity identifier, ID</typeparam>
    public interface IRepositoryWithTypedId<T, IdT>
    {
        /// <summary>
        /// Returns null if a row is not found matching the provided ID.
        /// </summary>
        T Get(IDataSource dataSource, IdT id);

        /// <summary>
        /// Returns all of the items of a given type.
        /// </summary>
        List<T> GetAll(IDataSource dataSource);

        /// <summary>
        /// Looks for zero or more instances using the <see cref="IDictionary{string, object}"/> provided.
        /// The key of the collection should be the property name and the value should be
        /// the value of the property to filter by.
        /// </summary>
        List<T> FindAll(IDataSource dataSource, IDictionary<string, object> propertyValuePairs);

        List<T> Search(IDataSource dataSource, IDictionary<string, object> propertyValuePairs);

        /// <summary>
        /// Looks for a single instance using the property/values provided.
        /// </summary>
        /// <exception cref="NonUniqueResultException" />
        T FindOne(IDataSource dataSource, IDictionary<string, object> propertyValuePairs);
        List<T> FindWithPaging(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);
        // List<T> FindWithPagingandInstitutionID(IDataSource dataSource, IDictionary<string, object> propertyValuePairs, int startIndex, int maxSize, out int total);
        /// <summary>
        /// Updates the data store with the changes made on the entity
        /// </summary>
        /// <param name="entity">Entity object to update</param>
        /// <returns>The updated Entity</returns>
        T Update(IDataSource dataSource, T entity);

        /// <summary>
        /// Merges the changes on the entity with that in the data store.
        /// </summary>
        /// <param name="entity">Entity object to Merge</param>
        /// <returns>Merged entity</returns>
        T Merge(IDataSource dataSource, T entity);


        /// <summary>
        /// Save  the entity into the data store
        /// </summary>
        /// <param name="entity">Entity object to save</param>
        /// <returns>Saved entity with the generated ID</returns>
        T Save(IDataSource dataSource, T entity);

        /// <summary>
        /// De-associate entity with the session
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="entity"></param>
        void Evict(IDataSource dataSource, T entity);

        /// <summary>
        /// I'll let you guess what this does.
        /// </summary>
        void Delete(IDataSource dataSource, T entity);

        object ExecuteSqlQuery(IDataSource dataSource, string query);

        void UpdateSqlQuery(IDataSource dataSource, string query);

        /// <summary>
        /// Provides a handle to application wide DB activities such as committing any pending changes,
        /// beginning a transaction, rolling back a transaction, etc.
        /// </summary>
        IDbContext DbContext { get; }
    }
}
