﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BAMS.Payments.Service
{
    public class InitRoute
    {

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            //config.Routes.MapHttpRoute(name: "Default",
            //    routeTemplate: "{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //    );
            config
               .Routes.MapHttpRoute(name: "Api",
               routeTemplate: "{controller}/",
               defaults: new { id = RouteParameter.Optional }
                   );
            config.MapHttpAttributeRoutes();

            app.UseWebApi(config);

        }
    }
}
