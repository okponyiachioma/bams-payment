﻿using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Logic;
using BAMS.Payments.Logic.Initializer;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BAMS.Payments.Service
{
    public class StartUp
    {
        Logger Logger = new Logger();
        IDisposable webApp;
        private Timer timer = null;
        private bool isBusy = false;
       

       public void OnStart()
        {
            Logger.Log(string.Format("{0}----Windows Service Starting..", DateTime.Now));
            try
            {
                isBusy = true;
                
                Logger.Log(string.Format("{0}----Starting Initialization..", DateTime.Now));

                var ssl = new ServiceInitialization();
                Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);

                ApplicationInitializer.Init();

                Logger.Log(string.Format("{0}---Service Started..", DateTime.Now));

                string baseAddress = ConfigurationManager.AppSettings["baseAddress"];
                webApp = WebApp.Start<InitRoute>(url: baseAddress);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw;
            }
            isBusy = false;

            this.timer = new Timer();
            this.timer.Elapsed += new ElapsedEventHandler(this.timer_Elapsed);
            this.timer.Start();

        }

       public void OnStop()
        {
            if (webApp != null)
            {
                webApp.Dispose();
            }
            Logger.Log(string.Format("{0}----Service Stopped..", DateTime.Now));

        }
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //System.Diagnostics.Trace.TraceInformation(string.Format("Timer just elapsed. Is busy is: {0}", isBusy));
            if (!isBusy)
            {
                this.timer.Stop();
                Logger.Log(("Process Started"));
                try
                {
                    new PaymentLogic().ProcessLoggedTransaction();
                }
                catch (Exception ex)
                {
                    Logger.Log(($"Process Error {ex.Message} \n {ex.StackTrace}"));
                }
                finally
                {
                    Logger.Log(("Process Ended"));
                    double.TryParse(System.Configuration.ConfigurationManager.AppSettings["TimerInterval"], out double interval);

                    this.timer.Interval = interval * 1000;
                    this.timer.Start();
                }

            }
        }

        public void ConsoleRun()
        {
            try
            {
                Logger.Log(string.Format("{0}----Service Starting..", DateTime.Now));
                OnStart();
                Logger.Log(string.Format("{0}---Service Started..", DateTime.Now));
            }
            catch (Exception ex)
            {

            }

        }

    }
}
