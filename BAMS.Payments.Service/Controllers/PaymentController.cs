﻿using BAMS.Payments.Core.Exceptions;
using BAMS.Payments.Core.Implementation;
using BAMS.Payments.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BAMS.Payments.Service.Controllers
{
    [RoutePrefix("Payment")]
    public class PaymentController : ApiController
    {


        private AuthenticationHeader GetHeadersValues(HttpRequestMessage request)
        {
            AuthenticationHeader header = new AuthenticationHeader();
            try
            {
                System.Net.Http.Headers.HttpRequestHeaders headers = request.Headers;
                if (!headers.Contains("clientcode") || !headers.Contains("mac") || !headers.Contains("timestamp"))
                    throw new AuthenticationException("Unauthorized Request");

                header.ClientCode = headers.GetValues("clientcode").FirstOrDefault();
                header.Mac = headers.GetValues("mac").FirstOrDefault();
                header.TimeStamp = headers.GetValues("timestamp").FirstOrDefault();

            }

            catch (Exception ex)
            {
                throw new AuthenticationException(ex.Message);
            }
            return header;
        }

        [Route("name-enquiry")]
        [HttpPost]
        public IHttpActionResult NameEnquiry2(AccountEnquiryRequest request)
        {
            PaymentLogic logic = new PaymentLogic();
            try
            {
                var header = GetHeadersValues(Request);
                //validate
                if (!logic.Authenticate(header))
                {
                    return StatusCode(System.Net.HttpStatusCode.Unauthorized);
                }
                if (request == null)
                {
                    return BadRequest();
                }
                var response = logic.NameEnquiry(request);
                //if (response.IsSuccessful)
                //{
                return Ok(response);
                //}
            }
            catch (AuthenticationException ex)
            {
                HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
                return ResponseMessage(_response);
            }

        }

        [Route("balance")]
        [HttpPost]
        public IHttpActionResult BalanceEnquiry(AccountEnquiryRequest request)
        {
            PaymentLogic logic = new PaymentLogic();
            try
            {
                var header = GetHeadersValues(Request);
                //validate
                if (!logic.Authenticate(header))
                {
                    return StatusCode(System.Net.HttpStatusCode.Unauthorized);
                }
                if (request == null)
                {
                    return BadRequest();
                }
                var response = logic.BalanceEnquiry(request);
                //if (response.IsSuccessful)
                //{
                return Ok(response);
                //}
                //HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                //return ResponseMessage(_response);
            }
            catch (AuthenticationException ex)
            {
                HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
                return ResponseMessage(_response);
            }
        }

        [Route("fundstransfer")]
        [HttpPost]
        public IHttpActionResult Transfer(TransferRequest request)
        {
            PaymentLogic logic = new PaymentLogic();
            try
            {
                var header = GetHeadersValues(Request);
                //validate
                if (!logic.Authenticate(header))
                {
                    return StatusCode(System.Net.HttpStatusCode.Unauthorized);
                }
                if (request == null || request.creditaccounts == null || request.creditaccounts.Count == 0 || string.IsNullOrEmpty(request.debitaccountnumber))
                {
                    return BadRequest();
                }
                var response = logic.LogTransferRequest(request);

                var theResponse = Request.CreateResponse(HttpStatusCode.Accepted, response);
                return ResponseMessage(theResponse);

                //HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                //return ResponseMessage(_response);
            }
            catch (AuthenticationException ex)
            {
                HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
                return ResponseMessage(_response);
            }
        }

        [Route("transaction-status")]
        [HttpPost]
        public IHttpActionResult TSQ(TSQRequest request)
        {
            PaymentLogic logic = new PaymentLogic();
            try
            {
                var header = GetHeadersValues(Request);
                //validate
                if (!logic.Authenticate(header))
                {
                    return StatusCode(System.Net.HttpStatusCode.Unauthorized);
                }
                if (request == null)
                {
                    return BadRequest();
                }
                var response = logic.TSQ(request);
                //if (response.IsSuccessful)
                //{
                return Ok(response);
                //}
                //HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                //return ResponseMessage(_response);
            }
            catch (AuthenticationException ex)
            {
                HttpResponseMessage _response = Request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
                return ResponseMessage(_response);
            }
        }
    }
}
