﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Hibernate.Implementation
{
    public class DataSourceFactory
    {

        public static IDataSource GetDataSource(DataCategory dataCategory)
        {
            return new DataSource(dataCategory, Guid.NewGuid().ToString());
        }


    }
}
