﻿using FluentNHibernate;
using FluentNHibernate.Automapping;
using NHibernate;
using NHibernate.Cfg;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BAMS.Payments.Data.Hibernate.Implementation
{
    /// <summary>
    /// Provides a standard interface for managing session storage
    /// </summary>
    public interface ISessionStorage
    {
        ISession Session { get; set; }
    }

    /// <summary>
    /// Basic implementation of <see cref="ISessionStorage"/> using in-memory storage
    /// </summary>
    public class SimpleSessionStorage : ISessionStorage
    {
        public ISession Session { get; set; }
    }

    /// <summary>
    /// Main class for setting up and initializing the NHibernate Configurations, Settings, SessionFactories, ...
    /// </summary>
    public static class HibernateHelper
    {
       

        private static readonly Dictionary<string, ISessionFactory> SessionFactories =
            new Dictionary<string, ISessionFactory>();

        private static readonly Dictionary<string, ISessionStorage> SessionStorages =
            new Dictionary<string, ISessionStorage>();



        public static Configuration Init(ISessionStorage storage, string[] mappingAssemblies, AutoPersistenceModel autoPersistenceModel, string cfgFile, IDictionary<string, string> cfgProperties, IDataSource dataSource)
        {
            //Pre-condition checks
            if (storage == null) throw new ArgumentNullException("Storage");
            if (dataSource == null) throw new ArgumentNullException("DataSource");

            
            System.Threading.Thread.Sleep(600);
            try
            {
                if (!SessionFactories.ContainsKey(dataSource.FactoryKey))
                {
                    Configuration cfg = ConfigureNHibernate(cfgFile, cfgProperties);
                    AddMappingAssembliesTo(cfg, mappingAssemblies, autoPersistenceModel);
#if DEBUG
                    //Auto-update the Database with missing Tables/Columns
                    new global::NHibernate.Tool.hbm2ddl.SchemaUpdate(cfg).Execute(false, true);
#endif
                    SessionFactories.Add(
                          dataSource.FactoryKey,
                          cfg.BuildSessionFactory());

                    SessionStorages.Add(
                          dataSource.StorageKey, storage
                          );
                    return cfg;
                }
            }
            catch (HibernateException ex)
            {
                throw new HibernateException("Cannot create session factory.", ex);
            }
            return null;
        }

       

        /// <summary>
        /// Register an interceptor to be used with the NHibernate SessionFactory
        /// </summary>
        /// <param name="interceptor"></param>
        public static void RegisterInterceptor(IInterceptor interceptor)
        {      
            RegisteredInterceptor = interceptor;
        }

        /// <summary>
        /// The NHibernate SessionFactory
        /// </summary>
        public static ISessionFactory SessionFactory { get; set; }

        /// <summary>
        /// The Session Storage Object
        /// </summary>
        public static ISessionStorage Storage { get; set; }

        public static void Close(IDataSource dataSource)
        {
            ISessionStorage storage = null;

            if (SessionStorages.ContainsKey(dataSource.StorageKey))
            {
                storage = SessionStorages[dataSource.StorageKey];
            }

            if (storage != null)
            {
                //Get the current session from the storage object
                ISession session = storage.Session;
                if (session != null)
                {
                    if (session.Transaction != null && session.Transaction.IsActive && !session.Transaction.WasCommitted && !session.Transaction.WasRolledBack)
                    {
                        session.Transaction.Commit();
                        session.Flush();
                    }
                    session.Close();
                }
                storage.Session = null;
            }
        }

        public static void CloseSimpleStorage(IDataSource dataSource)
        {
            ISessionStorage storage = null;

            if (SessionStorages.ContainsKey(dataSource.StorageKey))
            {
                storage = SessionStorages[dataSource.StorageKey];
            }

            if (storage != null && storage is SimpleSessionStorage)
            {
                //Get the current session from the storage object
                ISession session = storage.Session;
                if (session != null && !session.Transaction.IsActive)
                {
                    session.Close();
                    storage.Session = null;
                }
            }
        }
        /// <summary>
        /// The Current NHibernate Session
        /// </summary>

        public static ISession Current(IDataSource dataSource)
        {
            ISessionStorage storage = null;
            //Dictionary<string, ISessionStorage> _SessionStorages =   new Dictionary<string, ISessionStorage>();

            if (SessionStorages.ContainsKey(dataSource.StorageKey))
            {
                storage = SessionStorages[dataSource.StorageKey];
            }
            if (storage == null)
            {
                if (HttpContext.Current?.ApplicationInstance == null)
                {
                    storage = new SimpleSessionStorage();
                }
                else
                {
                    storage = new WebSessionStorage(HttpContext.Current.ApplicationInstance, dataSource);
                }
                SessionStorages.Add(dataSource.StorageKey, storage);
            }
            //Get the current session from the storage object
            ISession session = storage.Session;

            //Start a new session if no current session exists
            if (session == null || !session.IsOpen)
            {
                //Apply the interceptor if any was registered and open the session
                if (RegisteredInterceptor != null)
                {
                    session = SessionFactories[dataSource.FactoryKey].OpenSession(RegisteredInterceptor);
                }
                else
                {
                    session = SessionFactories[dataSource.FactoryKey].OpenSession();
                }
                //Begin a transaction
                if (storage is WebSessionStorage)
                {
                    session.BeginTransaction();
                }
                //Update the storage with the new session
                storage.Session = session;
            }
            return session;
        }

        private static void AddMappingAssembliesTo(Configuration cfg, ICollection<string> mappingAssemblies, AutoPersistenceModel autoPersistenceModel)
        {
            //if (mappingAssemblies != null && mappingAssemblies.Count >= 1) throw new ArgumentNullException(
            //    "mappingAssemblies must be provided as a string array of assembly names which contain mapping artifacts. " +
            //    "The artifacts themselves may be HBMs or ClassMaps.  You may optionally include '.dll' on the assembly name.");

            foreach (string mappingAssembly in mappingAssemblies)
            {
                if (String.IsNullOrEmpty(mappingAssembly.Trim())) continue;

                string loadReadyAssemblyName = (mappingAssembly.IndexOf(".dll") == -1)
                    ? mappingAssembly.Trim() + ".dll"
                    : mappingAssembly.Trim();

                Assembly assemblyToInclude = Assembly.LoadFrom(loadReadyAssemblyName);
                // Looks for any HBMs
                cfg.AddAssembly(assemblyToInclude);

                if (autoPersistenceModel == null)
                {
                    // Looks for any Fluent NHibernate ClassMaps
                    cfg.AddMappingsFromAssembly(assemblyToInclude);
                }
                else
                {
                    autoPersistenceModel.AddMappingsFromAssembly(assemblyToInclude);
                    cfg.AddAutoMappings(autoPersistenceModel);

                }
            }
        }

        /// <summary>
        /// Gets the Nhibernate configuration from the file and applies the properties specified to it
        /// </summary>
        /// <param name="cfgFile">The Nhibernate Configuration filepath</param>
        /// <param name="cfgProperties">Configuration properties to apply</param>
        /// <returns>Nhibernate Configuration Object</returns>
        private static Configuration ConfigureNHibernate(string cfgFile, IDictionary<string, string> cfgProperties)
        {
            Configuration cfg = new Configuration();

            if (string.IsNullOrEmpty(cfgFile))
            {
                cfg = cfg.Configure();
            }
            else
            {
                cfg = cfg.Configure(cfgFile);
            }

            if (cfgProperties != null)
            {
                foreach (string key in cfgProperties.Keys)
                {
                    cfg.Properties.Remove(key);
                }
                cfg.AddProperties(cfgProperties);
            }

            return cfg;
        }

       

        /// <summary>
        /// The interceptor to apply when a session is created
        /// </summary>
        private static IInterceptor RegisteredInterceptor;
    }
}
