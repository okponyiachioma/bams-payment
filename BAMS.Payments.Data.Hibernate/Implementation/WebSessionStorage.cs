﻿using NHibernate;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BAMS.Payments.Data.Hibernate.Implementation
{
    /// <summary>
    /// Implements <see cref="ISessionStorage"/> via the Open-Session-in-View pattern
    /// Handles storage of Nhibernate Storage using the HttpContext and Closes the session at the end of the HttpRequest.
    /// </summary>
    public class WebSessionStorage : ISessionStorage
    {
        /// <summary>
        /// Constant key for storing the session in the HttpContext
        /// </summary>
        private const string CurrentSessionKey = "nhibernate.current_session";
        private IDataSource _dataSource;
        private HttpApplication _app;

        /// <summary>
        /// Initialize the storage for the specified Web Application
        /// </summary>
        /// <param name="app">The HttpApplication of the calling web application</param>
        public WebSessionStorage(HttpApplication app)
        {
            app.EndRequest += Application_EndRequest;
        }

        public WebSessionStorage(HttpApplication app, IDataSource dataSource)
        {
            _app = app;
            _dataSource = dataSource;
            //_app.EndRequest += Application_EndRequest;
        }

        public ISession Session
        {
            get
            {
                //Get the current session from the HttpContext Variable
                HttpContext context = HttpContext.Current;
                ISession session = context.Items[CurrentSessionKey + _dataSource.StorageKey] as ISession;
                return session;
            }
            set
            {
                //Saves the current session into the HttpContext
                HttpContext context = HttpContext.Current;
                context.Items[CurrentSessionKey + _dataSource.StorageKey] = value;
            }
        }

        void Application_EndRequest(object sender, EventArgs e)
        {
            ISession session = Session;

            //Closes the session if there's any open session
            if (session != null)
            {
                if (session.Transaction != null && session.Transaction.IsActive && !session.Transaction.WasCommitted && !session.Transaction.WasRolledBack)
                {
                    // session.Transaction.Commit();
                    // session.Flush();
                }
                //session.Close();
                session.Dispose();
                HttpContext context = HttpContext.Current;
                context.Items.Remove(CurrentSessionKey + _dataSource.StorageKey);
            }
        }
    }
}
