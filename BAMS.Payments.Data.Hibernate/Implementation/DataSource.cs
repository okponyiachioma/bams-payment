﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Helpers;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Hibernate.Implementation
{
    public class DataSource : IDataSource
    {
       

        internal DataSource(DataCategory DataCategory, string sessionKey)
        {
            _SessionKey = sessionKey;
            this.DataCategory = DataCategory;
        }

  

        public DataCategory DataCategory { get; protected set; }

        public string FactoryKey
        {

            get
            {
                string factoryKey = this.DataCategory.ToString();
                
                return factoryKey;
            }
        }

        public string SessionKey
        {
            get
            {
                return _SessionKey;
            }
        }

        public string StorageKey
        {
            get
            {
                string storageKey = this.DataCategory.ToString();
                if (!string.IsNullOrEmpty(_SessionKey))
                {
                    storageKey += string.Format("_{0}", _SessionKey);
                }
                return storageKey;
            }
        }

        private string _SessionKey;

        public override int GetHashCode()
        {
            return (this.FactoryKey + this.SessionKey).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj != null)
            {
                IDataSource comp = (IDataSource)obj;
                if (comp != null)
                {
                    if (comp.FactoryKey == this.FactoryKey && comp.SessionKey == this.SessionKey)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        private IDbContext _dbContext = SafeServiceLocator<IDbContext>.GetService();
        public void Dispose()
        {
            //Trace.TraceInformation("About to dispose datasource: {0}", StorageKey);
            _dbContext.Close(this);
            //Trace.TraceInformation("Disposed datasource: {0}", StorageKey);
        }
    }

}
