﻿using NHibernate;
using BAMS.Payments.Core.Enums;
using BAMS.Payments.Data.Hibernate.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Hibernate.Implementation
{
    /// <summary>
    /// This has to implement <see cref="IDbContext" />, but there's no reason for it to be 
    /// recreated more than once.  Therefore, it's been setup as a threadsafe singleton.
    /// Singleton guidance from http://www.yoda.arachsys.com/csharp/singleton.html.
    /// </summary>
    public sealed class DbContext : IDbContext
    {
        public DbContext() { }

        /// <summary>
        /// Singleton Instance of <see cref="DbContext"/>
        /// </summary>
        public static DbContext Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested() { }
            internal static readonly DbContext instance = new DbContext();
        }

        private ISession Session(IDataSource dataSource)
        {
            return HibernateHelper.Current(dataSource);
        }


        /// <summary>
        /// This isn't specific to any one DAO and flushes everything that has been 
        /// changed since the last commit.
        /// </summary>
        public void CommitChanges(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).IsOpen)
            {
                Session(dataSource).Flush();
            }
        }

        public void CloseSources(IList<IDataSource> sources)
        {
            foreach (IDataSource source in sources)
            {
                Close(source);
            }
        }
        public void Terminate()
        {
            IList<IDataSource> sources = new List<IDataSource>();

            IDataSource coreSource = DataSourceFactory.GetDataSource(DataCategory.Core);
            sources.Add(coreSource);
            CloseSources(sources);

        }
        public void Close(IDataSource dataSource)
        {
            HibernateHelper.Close(dataSource);
        }

        public void BeginTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction == null || !Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).BeginTransaction();
            }
        }

        public void CommitTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction != null && Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).Transaction.Commit();
            }
        }

        public void RollbackTransaction(IDataSource dataSource)
        {
            //lock (Session(dataSource))
            if (Session(dataSource).Transaction != null && Session(dataSource).Transaction.IsActive)
            {
                Session(dataSource).Transaction.Rollback();
            }
        }

       
    }
}
