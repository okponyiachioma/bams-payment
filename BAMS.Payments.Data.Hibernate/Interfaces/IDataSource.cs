﻿using BAMS.Payments.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Data.Hibernate.Interfaces
{
  public  interface IDataSource:IDisposable
    {
        DataCategory DataCategory { get; }

        string FactoryKey { get; }

        string SessionKey { get; }

        string StorageKey { get; }
    }
}
