﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
        void LogException(Exception ex);
    }
}
