﻿using BAMS.Payments.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Interfaces
{
   public interface ITransaction:IEntity
    {
        DateTime LogDate { get; set; }
        DateTime? DateUpdated { get; set; }     
        string ResponseCode { get; set; }
        string ResponseMessage { get; set; }
        TransactionStatus Status { get; set; }
    }
}
