﻿using BAMS.Payments.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Interfaces
{
   public interface ICreditAccount:IEntity
    {
        string accountnumber { get; set; }
        string bankcode { get; set; }
        string narration { get; set; }
        string serialno { get; set; }
        decimal amount { get; set; }
        string status { get; set; }
       string statusdescription { get; set; }
        TransactionStatus TransactionStatus { get; set; }
        ITransferRequest TransferRequest { get; set; }
        string transactionRef { get; set; }

    }
}
