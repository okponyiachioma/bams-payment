﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Interfaces
{
    public interface ITransferRequest : IEntity, ITransaction
    {

        string description { get; set; }
        string transref { get; set; }
        string debitaccountnumber { get; set; }
        string debitbankcode { get; set; }
        string mfbhandlesdebit { get; set; }
        IList<CreditAccount> creditaccounts { get; set; }
        string paymentref { get; set; }
        TransactionType transactionType { get; set; }


    }

}
