﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BAMS.Payments.Core.Implementation
{
    public class TransferRequest : Transaction, ITransferRequest
    {
        public virtual string description { get; set; }
        public virtual string transref { get; set; }
        public virtual string debitaccountnumber { get; set; }
        public virtual string debitbankcode { get; set; }
        public virtual string mfbhandlesdebit { get; set; }
        public virtual IList<CreditAccount> creditaccounts { get; set; }
        public virtual string paymentref { get; set; }
        public virtual TransactionType transactionType { get; set; }
    }
}
