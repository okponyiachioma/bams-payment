﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    public  class TransferResponse
    { 
        public virtual string transref { get; set;}
        public virtual string paymentref { get; set;}
        public virtual string status { get; set;}
        public virtual string statusdescription { get; set;}
        public virtual string responsetimestamp { get; set;}
        public virtual IList<CreditAccount> creditaccounts { get; set; }
        public bool IsSuccessful { get; set; }
    }
}
