﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    public class Transaction : Entity, ITransaction
    {
        public virtual DateTime LogDate {get; set;}
        public virtual DateTime? DateUpdated { get; set; }
        public virtual string ResponseCode {get; set;}
        public virtual string ResponseMessage {get; set;}
        public virtual TransactionStatus Status {get; set;}
    }
}
