﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
   public  class AuthenticationHeader
    {
        public string ClientCode { get; set; }
        public string Mac { get; set; }
        public string TimeStamp { get; set; }
    }
}
