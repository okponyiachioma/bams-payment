﻿using BAMS.Payments.Core.Enums;
using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    [DataContract]
    public class CreditAccount : Entity, ICreditAccount
    {
        [DataMember]
        public virtual string accountnumber { get; set; }
        [DataMember]
        public virtual string bankcode { get; set; }
        [DataMember]
        public virtual string narration { get; set; }
        [DataMember]
        public virtual string serialno { get; set; }
        [DataMember]
        public virtual decimal amount { get; set; }
        [DataMember]
        public virtual string status { get; set; }
        public virtual TransactionStatus TransactionStatus { get; set; }
        [DataMember]
        public virtual string statusdescription { get; set; }
        [IgnoreDataMember]
        public virtual ITransferRequest TransferRequest { get; set; }
        [IgnoreDataMember]
        public virtual string transactionRef { get; set; }
    }
}
