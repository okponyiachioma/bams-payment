﻿using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    public class Entity : IEntity
    {
        public virtual long ID {get; set; }
    }
}
