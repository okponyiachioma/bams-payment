﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
   public class Logger
    {
        public static void Log(string message)
        {
            Trace.TraceInformation(message);
        }

        public static void Log(Exception ex, string messageBefore = "")
        {
            Trace.TraceInformation(messageBefore + ":\n" + getInnerMessage(ex));
        }
        private static string getInnerMessage(Exception ex)
        {
            string message = string.Empty;

            while (ex != null)
            {
                message += ex.Message + "\n";
                ex = ex.InnerException;
            }
            return message;
        }
    }
}
