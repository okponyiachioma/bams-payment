﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    [Serializable]
    [DataContract]
   public class NameEnquiryResponse
    {
        [DataMember]
        public string ResponseCode { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string BVN { get; set; }
        [DataMember]
        public string KYC { get; set; }
        [DataMember]
        public string SessionID { get; set; }
        [DataMember]
        public string DestinationBankCode { get; set; }
        [DataMember]
        public string ChannelCode { get; set; }
    }

}
