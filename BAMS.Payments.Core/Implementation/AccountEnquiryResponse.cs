﻿using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BAMS.Payments.Core.Implementation
{
    public class AccountEnquiryResponse
    {
        public string accountnumber { get; set; }
        public string balance { get; set; }
        public string responsetimestamp { get; set; }
        public string accountname { get; set; }
        public bool IsSuccessful { get; set; }

    }
}



