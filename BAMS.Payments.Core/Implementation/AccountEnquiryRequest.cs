﻿using BAMS.Payments.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BAMS.Payments.Core.Implementation
{
    public class AccountEnquiryRequest : Transaction, IAccountEnquiryRequest
    {
        public virtual string accountnumber { get; set; }
    }
}
