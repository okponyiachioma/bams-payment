﻿using BAMS.Payments.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Implementation
{
    [Serializable]
    [DataContract]
   public  class Response
    {
        [DataMember]
        public string ResponseCode { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }
        public TransactionStatus Status { get; set; }
    }
}
