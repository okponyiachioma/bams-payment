﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Helpers
{
    public static class SafeServiceLocator<TDependency>
    {
        public static TDependency GetService()
        {
            TDependency service;

            service = (TDependency)SafeServiceLocator.GetService(typeof(TDependency));

            return service;
        }
    }

    public static class SafeServiceLocator
    {
        public static object GetService(Type theType)
        {
            object service;

            try
            {
                service = ServiceLocator.Current.GetService(theType);
            }
            catch (NullReferenceException)
            {
                return null;
            }
            catch (ActivationException)
            {
                throw new ActivationException("The needed dependency of type " + theType.Name +
                    " could not be located with the ServiceLocator. You'll need to register it with " +
                    "the Common Service Locator (CSL) via your IoC's CSL adapter.");
            }

            return service;
        }
    }
}
