﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BAMS.Payments.Core.Helpers
{
    public class XMLHelper
    {
        public T ConvertToObject<T>(string rawxml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(new StringReader(rawxml));
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
    }
}
