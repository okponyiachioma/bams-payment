﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Exceptions
{
    public class FailureException : ApplicationException
    {
        public string Code { get; set; }
        public FailureException(string msg)
            : base(msg)
        {

        }

        public FailureException(string msg, string code)
         : base(msg)
        {
            Code = code;
        }
    }
}
