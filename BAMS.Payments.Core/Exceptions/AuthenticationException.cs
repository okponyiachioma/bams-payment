﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public string Code { get; set; }
        public AuthenticationException(string msg)
            : base(msg)
        {

        }

        public AuthenticationException(string msg, string code)
         : base(msg)
        {
            Code = code;
        }
    }
}
