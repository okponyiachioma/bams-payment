﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Enums
{
    public enum TransactionType
    {
        Debit=1,
        Credit=2
    }
}
