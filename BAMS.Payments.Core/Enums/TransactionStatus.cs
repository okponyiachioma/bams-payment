﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAMS.Payments.Core.Enums
{
   public enum TransactionStatus
    {
        Pending=1,
        Successful=2,
        Failed=3,
        Reversed=4,
        ReadyForProcessing=5,
        Processing=6,
        Suspended=7,
        Processed=8
    }
}
